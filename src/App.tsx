import React from "react";
import ReactDom from "react-dom";
import logo from "./logo.svg";
import "./App.css";
import { Layout, Menu, Icon, PageHeader } from "antd";
import "antd/dist/antd.css";
import { Provider } from "react-redux";
import Login from "./login";


// @ts-ignore
import configureServiceStore from "mattermost-redux/store";

// @ts-ignore
import { Client4 } from "mattermost-redux/client";

const offlineOptions = {
  persistOptions: {
    autoRehydrate: {
      log: false
    }
  }
};
Client4.setUrl("https://communication.hotelsoft.tech");

const store = configureServiceStore({}, {}, offlineOptions);

interface IProps {}

interface Message {
  userId: number;
  userName: string;
  message: string;
  time: string;
}

interface channelList {
  channelList: { [k: string]: Message[] };
  activeChannel: string;
  showLogin: Boolean;
}

const { Header, Content, Footer, Sider } = Layout;
class App extends React.PureComponent<IProps, {}, channelList> {
  state = {
    showLogin: true,
    activeChannel: "",
    channelList: {
      channel1: [
        {
          userId: 123,
          userName: "user1",
          message: "qwre",
          time: new Date()
        }
      ],
      channel2: [
        {
          userId: 456,
          userName: "user2",
          message: "sdf",
          time: new Date()
        }
      ]
    }
  };
  changeChannel(e: any) {
    this.setState({ activeChannel: e.target.value });
    console.log(this.state.activeChannel);
  }
  render() {
    let channelNames = Object.keys(this.state.channelList);
    return (
      <div className="App">
        <Provider store={store}>
          <Layout>
            {this.state.showLogin ? (
              <div>
                <Login />
                {/* <Channel /> */}
              </div>
            ) : (
              <React.Fragment>
                
              </React.Fragment>
            )}
            ,
          </Layout>
        </Provider>
      </div>
    );
  }
}

export default App;
