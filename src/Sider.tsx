import React, { Component, PureComponent } from "react";
import { Layout, Menu, Icon, Input } from "antd";
import _ from "lodash";

export interface Message {
  userId: string;
  avatarUrl: string;
  userDisplayName: string;
  messageText: string;
  time: Date;
}
// export interface Channel {
//   id: string;
//   name: string;
//   iconName: string;
//   topic: string;
//   content: Message[];
// }

interface IProps {
  // channelNamesMatterMost: any;
  //channelNames: string[];
  channel_n: string[];
  activeChannelId: string;
  channelOnChange: (channel: string) => void;
  CreateChannel1: (channel_name: string) => void;
  deleteChannel1: () => void;
  getChannelMembers1: () => void;
}

interface IState {
  channel_name: string;
}

// export const staticData: Channel[] = [
//   {
//     id: "1",
//     name: "Typescript",
//     iconName: "user",
//     topic: "learn custom types",
//     content: [
//       {
//         userId: "111",
//         userDisplayName: "Lakkanna",
//         messageText: "Hi, there!",
//         avatarUrl:
//           "https://cdn3.iconfinder.com/data/icons/business-avatar-1/512/10_avatar-512.png",
//         time: new Date()
//       }
//     ]
//   },
//   {
//     id: "2",
//     name: "Redux",
//     iconName: "video-camera",
//     topic: "create common reducers for react and react native",
//     content: [
//       {
//         userId: "222",
//         userDisplayName: "Karthik",
//         messageText: "Hey, 😄",
//         avatarUrl:
//           "https://icon-library.net/images/avatar-icon-images/avatar-icon-images-4.jpg",
//         time: new Date()
//       }
//     ]
//   },
//   {
//     id: "3",
//     name: "React",
//     iconName: "upload",
//     topic: "migrate to new react version",
//     content: [
//       {
//         userId: "333",
//         userDisplayName: "Purushottam",
//         messageText: "Hello, 😎!",
//         avatarUrl:
//           "https://cdn.iconscout.com/icon/free/png-256/avatar-367-456319.png",
//         time: new Date()
//       }
//     ]
//   }
// ];

export default class Sider extends PureComponent<IProps, {}> {
  // static defaultProps = {
  //   channelNames: staticData
  // };
  state = {
    channel_name: ""
  };
  getActiveChannelId = (): string => {
    return this.props.activeChannelId
      ? this.props.activeChannelId
      : _.get(_.first(this.props.channel_n), "id", "1");
  };

  RenderChannelNames = (channel_n: string[]) => {
    // let i = 0;
    // if (i < 8) {
    return (
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={[this.getActiveChannelId()]}
      >
        {channel_n.map((channel_n, index) => (
          <Menu.Item
            key={index}
            onClick={() => this.props.channelOnChange(channel_n)}
            onMouseDown={this.props.getChannelMembers1}
          >
            <span
              className="nav-text"
              style={{ fontFamily: "OperatorMono-Book" }}
            >
              #{channel_n}{" "}
              <Icon type="close-circle" onClick={this.handleChannelDelete} />
            </span>
          </Menu.Item>
        ))}
      </Menu>
    );

    return null;
  };

  handleChannelDelete = () => {
    this.props.deleteChannel1();
  };

  handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e);
    this.setState({
      channel_name: e.target.value
    });
  };

  handleOnSubmit = () => {
    this.props.CreateChannel1(this.state.channel_name);
    this.setState({
      channel_name: ""
    });
  };

  render() {
    console.log(this.props.channel_n);
    return (
      <Layout.Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={broken => {
          console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type);
        }}
      >
        <div
          className="logo"
          style={{
            color: "#FFF",
            padding: 9,
            fontSize: 18,
            marginBottom: 6,
            fontWeight: 900,
            fontFamily: "Operator Mono"
          }}
        >
          <span style={{ fontSize: 24 }}> CHAT APP</span>
        </div>
        <div>
          <Input
            placeholder="Add Channel"
            value={this.state.channel_name}
            addonAfter={<Icon type="plus" onClick={this.handleOnSubmit} />}
            onChange={this.handleInputChange}
            title="Add Channel"
          />
        </div>
        {this.RenderChannelNames(this.props.channel_n)}
      </Layout.Sider>
    );
  }
}
