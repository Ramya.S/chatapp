import React, { Component } from "react";
import { Button } from "antd";
import { Layout, Menu, Icon, PageHeader } from "antd";

// state = {
//   channelNames: [
//     { channelId: 123, channelName: "kfsakjf" },
//     {  channelId: 124, channelName: "djfkaj"}
//   ],
//   channelsMessages: {

//   }

// }
interface IProps {}

interface IState {
  count: number;
  text: string;
}

export class AddChannel extends React.Component<IProps, IState> {
  constructor(props: any) {
    super(props);

    this.state = {
      count: 5,
      text: ""
    };
    this.onChange = this.onChange.bind(this);
    this.onAdd = this.onAdd.bind(this);
  }
  onChange(e: any) {
    this.setState({ text: e.target.value });
    console.log(this.state.text);
  }

  onAdd() {
    return (
      <Menu theme="dark" mode="inline" defaultSelectedKeys={["10"]}>
        <Menu.Item key="5">
          <Icon type="user" />
          <span className="nav-text">{this.state.text}</span>
        </Menu.Item>
      </Menu>
    );
  }

  render() {
    return (
      <div>
        <input
          onChange={e => this.onChange(e)}
          type="text"
          placeholder="Channel Name"
        />
        <Button type="primary" onClick={this.onAdd}>
          Add
        </Button>
      </div>
    );
  }
}

export default AddChannel;
