import React, { Component, PureComponent } from "react";
import { Layout, Input, Icon, Card, Avatar } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Message } from "./App1";
// @ts-ignore
import { getPosts } from "mattermost-redux/actions/posts";

interface ContentProps {
  channel_id1: string;
  //messages: Message[];
  handleOnNewMessage: (message: string, time: Date) => void;
  getPosts: (channel_id1: string) => void;
  messages_mattermost: {
    message: string;
    username: string;
    date: Date;
  }[];
  usersOfChannel: string[];
}

interface ContentState {
  newMessage: string;
  messages_new: string[];
}

class Content extends PureComponent<ContentProps, ContentState> {
  state = {
    newMessage: "",
    messages_new: [""]
  };

  handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e);
    this.setState({
      newMessage: e.target.value
    });
  };

  handleOnSubmit = () => {
    this.props.handleOnNewMessage(this.state.newMessage, new Date());
    this.setState({
      newMessage: ""
    });
  };
  getPosts1 = async () => {
    console.log("inside");
    console.log(this.props.channel_id1);
    const posts1: any = await this.props.getPosts(this.props.channel_id1);
    console.log(posts1);
    const m = posts1.data.posts;
    console.log(m);
    let arr = [];
    for (var prop in m) {
      var msg = m[prop]["message"];
      arr.push(msg);
    }
    console.log("................");
    console.log(arr);
    this.setState({
      messages_new: arr
    });
    console.log(this.state.messages_new);
  };
  render() {
    //console.log(this.props.messages, this.state.newMessage);
    return (
      <Layout.Content style={{ margin: "24px 16px 0" }}>
        <div>
          {this.props.messages_mattermost
            ? this.props.messages_mattermost.map(msg => {
                return (
                  <div>
                    {" "}
                    
                    <Card 
                      size="small"
                      title={`${msg.date} : ${msg.username}`}
                      extra={<a href="#">More</a>}
                      style={{ width: 1000 }}
                    >
                      <Avatar
                      style={{ backgroundColor: "#87d068", marginRight:"1200px" }}
                      icon="user"
                    />
                      {msg.message}{" "}
                    </Card>
                    <hr />
                  </div>
                );
              })
            : "Messages are loading...."}
        </div>
        {/* <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
          {this.props.messages
            ? this.props.messages.map(msg => {
                return <div> {`${msg.displayName}  ${msg.message}`} </div>;
              })
            : "Messages are loading...."}
        </div> */}
        <div style={{ position: "absolute", right: 0, top: 100 ,backgroundColor:"blue"}}><p style={{color:"white"}}>USERS OF CHANNEL</p>
          {this.props.usersOfChannel
            ? this.props.usersOfChannel.map(usr => {
                return <ul>{usr}</ul>;
              })
            : "Users are loading"}
        </div>
        <Input
          value={this.state.newMessage}
          addonAfter={<Icon type="message" onClick={this.handleOnSubmit} />}
          onChange={this.handleInputChange}
        />
      </Layout.Content>
    );
  }
}
const mapStateToProps = (state: any) => {
  console.log("State: ", state);
  return {};
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getPosts
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Content);
