import React, { Component , PureComponent} from "react";
import { connect } from "react-redux";
import { Layout, Button } from "antd";
import { bindActionCreators } from "redux";
// @ts-ignore
import { login, getUser } from "mattermost-redux/actions/users"; // importing the action
import { genPercentAdd } from "antd/lib/upload/utils";

interface HeaderProps {
  channelName: string;
  channelTopic: string;
  login?: any;
  getUser?: any;
  logout1: () => void;
}

class Header extends PureComponent<HeaderProps, {}> {
  static defaultProps = {
    channelName: "Channel Name loading...",
    channelTopic: "Channel topic loading..."
  };

  componentDidMount() {
    this.props.login("ramya", "ramya");
    const user = this.props.getUser();
    console.log("User: ", user);
  }

  render() {
    return (
      <Layout.Header
        style={{
          background: "#fff",
          padding: 10,
          fontSize: 16,
          fontFamily: "OperatorMono-Medium"
        }}
      >
        <div style={{ display: "flex", flexDirection: "row" }}>
          <div
            style={{
              fontFamily: "OperatorMono-Bold",
              fontSize: 30,
              color: "blue",
              paddingRight: 10
            }}
          >
            #{this.props.channelName}
          </div>
          <Button
            type="danger"
            style={{ position: "absolute", right: 0 }}
            onClick={this.props.logout1}
          >
            LOGOUT
          </Button>
        </div>
      </Layout.Header>
    );
  }
}

const mapStateToProps = (state: any) => {
  console.log(state);
  return {};
};

const mapDispatchToProps = (dispatch: any) => {
  console.log("dispatch: ", dispatch);
  return bindActionCreators(
    {
      login,
      getUser // passing the action as a prop
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);
