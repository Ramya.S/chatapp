import React, { Component, PureComponent } from "react";
import { Layout } from "antd";
import _ from "lodash";
import SiderView from "./Sider";
import HeaderView from "./Header";
import ContentView from "./Content";
import "antd/dist/antd.css";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// @ts-ignore
import { getPosts, createPost } from "mattermost-redux/actions/posts";
//import { string } from "prop-types";

// @ts-ignore
import {
  createChannel,
  deleteChannel,
  getChannelMembers
  // @ts-ignore
} from "mattermost-redux/actions/channels";

// @ts-ignore
import { getUser } from "mattermost-redux/actions/users";

interface IndexSignature {
  [key: string]: any;
}
export interface Message {
  userId: number;
  message: string;
  displayName: string;
  time: Date;
}
interface ChannelDetails {
  [key: string]: Message[];
}
interface NewAppState {
  activeChannelName: string;
  //channelDetails: ChannelDetails;
  channel_names: string[];
  channel_id: string;
  messages_new: {
    message: string;
    username: string;
    date: Date;
  }[];
  userId: string;
  usersOfChannel: string[];
}
interface IProps {
  state1: any;
  getPosts: (channel_id1: string) => void;
  createPost: (post: any) => void;
  logout1: () => void;
  createChannel: (channel: string, userId: string) => void;
  deleteChannel: (channelId: string) => void;
  getChannelMembers: (channelId: string) => void;
  getUser: (userId: string) => void;
}

class NewApp extends PureComponent<IProps, NewAppState> {
  state = {
    activeChannelName: "",
    channel_id: "",
    usersOfChannel: [""],
    date_milli: 0,
    channel_names: [""],
    messages_new: [
      {
        message: "",
        username: "",
        date: new Date()
      }
    ],
    userId: "",
    username: [""]

    // channelDetails: {
    //   general: [
    //     {
    //       userId: 1234,
    //       message: "message HELLO",
    //       displayName: "user1",
    //       time: new Date()
    //     }
    //   ],
    //   "ui-training": [
    //     {
    //       userId: 543532,
    //       message: "HELLO",
    //       displayName: "user2",
    //       time: new Date()
    //     }
    //   ]
    // }
  };

  componentDidMount() {
    const complete_channel = _.get(
      this.props.state1,
      "entities.channels.channels"
    );
    //console.log(complete_channel);
    //this.state.channel_id = Object.keys(complete_channel)[0];
    //const firstChannelName = complete_channel[prop]["display_name"];
    for (let i = 0; i < complete_channel.length; i++) {
      var firstChannelName = complete_channel[0]["display_name"];
    }

    //const firstChannelName = Object.keys(this.state.channelDetails)[0];
    this.setState({
      activeChannelName: firstChannelName ? firstChannelName : ""
    });
  }

  handleOnChannelChange = (channelName: string) => {
    //alert(channelName);
    this.setState({
      activeChannelName: channelName
    });
    this.getPosts1();
    const complete_channel = _.get(
      this.props.state1,
      "entities.channels.channels"
    );
    console.log(complete_channel);
    this.state.channel_id = Object.keys(complete_channel)[0];
    this.state.channel_names.splice(0);
    for (var prop in complete_channel) {
      var namee = complete_channel[prop]["display_name"];
      this.state.channel_names.push(namee);
    }

    for (var prop in complete_channel) {
      if (
        this.state.activeChannelName == complete_channel[prop]["display_name"]
      ) {
        this.state.channel_id = prop;
      }
    }
  };

  handleOnNewMessage = async (message: string, time: Date) => {
    console.log("I am here");

    this.setState({
      userId: this.props.state1.entities.users.currentUserId
    });

    const newMessage = {
      user_id: this.state.userId,
      channel_id: this.state.channel_id,
      message,
      props: {
        username: "ramya"
      }
    };
    await this.props.createPost(newMessage);
    this.getPosts1();
    // console.log(this.props.state1);
  };

  getPosts1 = async () => {
    console.log("inside");
    console.log(this.state.channel_id);
    const posts1: any = await this.props.getPosts(this.state.channel_id);
    console.log(posts1);
    const m = posts1.data.posts;
    console.log(m);

    let arrObj: {
      username: string;
      message: string;
      date: Date;
    }[] = [];

    let msguser: {
      username: string;
      message: string;
      date: Date;
    };

    for (var prop in m) {
      var msg = m[prop]["message"];
      var usr = m[prop]["props"].username;
      var date = m[prop]["create_at"];
      let newDate1 = new Date(date);
      msguser = {
        message: msg,
        username: usr,
        date: newDate1
      };
      arrObj.push(msguser);
    }

    console.log("................");
    console.log(arrObj);

    this.setState({
      messages_new: arrObj
    });
    console.log(this.state.messages_new);
  };

  createChannel1 = async (channel_name: string) => {
    console.log(channel_name);
    console.log(this.state.userId);
    await this.props.createChannel(channel_name, this.state.userId);
  };

  deleteChannel1 = async () => {
    await this.props.deleteChannel(this.state.channel_id);
  };

  getChannelMembers1 = async () => {
    let cm: any = await this.props.getChannelMembers(this.state.channel_id);
    let arr = [];
    for (let i = 0; i < cm.data.length; i++) {
      var id = cm.data[i].user_id;

      var user_name: any = await this.props.getUser(id);
      //var user_name1: any = _.get(user_name, "data.username");
      user_name = user_name.data.username;
      arr.push(user_name);
    }
    console.log(cm);
    console.log(arr);
    this.setState({ usersOfChannel: arr });
  };

  render() {
    //const channelNames = Object.keys(this.state.channelDetails);
    //const { channelDetails, activeChannelName } = this.state;
    //const a: IndexSignature = channelDetails;
    console.log("PROPS _________________:", this.props.state1);
    //const complete_channel = this.props.state1.entities.channels.channels;
    //console.log(complete_channel);
    const complete_channel = _.get(
      this.props.state1,
      "entities.channels.channels"
    );
    console.log(complete_channel);
    this.state.channel_id = Object.keys(complete_channel)[0];
    this.state.channel_names.splice(0);
    for (var prop in complete_channel) {
      var namee = complete_channel[prop]["display_name"];

      this.state.channel_names.push(namee);
    }

    for (var prop in complete_channel) {
      if (
        this.state.activeChannelName == complete_channel[prop]["display_name"]
      ) {
        this.state.channel_id = prop;
      }
    }
    console.log(this.state.channel_id);
    this.state.channel_names = this.state.channel_names.filter(
      (x, j, a) => a.indexOf(x) == j
    );

    console.log(this.state.channel_names);

    return (
      <Layout>
        <SiderView
          activeChannelId={this.state.channel_id}
          //channelNames={channelNames}
          channel_n={this.state.channel_names}
          channelOnChange={this.handleOnChannelChange}
          CreateChannel1={this.createChannel1}
          deleteChannel1={this.deleteChannel1}
          getChannelMembers1={this.getChannelMembers1}
        />
        <Layout>
          <HeaderView
            channelName={this.state.activeChannelName}
            logout1={this.props.logout1}
          />
          <ContentView
            channel_id1={this.state.channel_id}
            //messages={a[this.state.activeChannelName]}
            handleOnNewMessage={this.handleOnNewMessage}
            messages_mattermost={this.state.messages_new}
            usersOfChannel={this.state.usersOfChannel}
          />
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = (state: any) => {
  //console.log("State: ", state);
  return {};
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getPosts,
      createPost,
      createChannel,
      deleteChannel,
      getChannelMembers,
      getUser
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewApp);
