import React from "react";
import ReactDom from "react-dom";
import { Button } from "antd";

var text = "";
var messages = [""];

interface IProps {
  channelKey: number;
}

interface IState {
  channel1: [
    {
      channelName: string;
      message: any;
      userId: number;
      userDisplayName: string;
      time: string;
    }
  ];
  channel2: [
    {
      channelName: string;
      message: string;
      userId: number;
      userDisplayName: string;
      time: string;
    }
  ];
  channel3: [
    {
      channelName: string;
      message: string;
      userId: number;
      userDisplayName: string;
      time: string;
    }
  ];

  channel4: [
    {
      channelName: string;
      message: string;
      userId: number;
      userDisplayName: string;
      time: string;
    }
  ];

  [k: string]: [
    {
      channelName: string;
      message: string;
      userId: number;
      userDisplayName: string;
      time: string;
    }
  ];
}

export class Messages extends React.Component<IProps, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      //channel: `channel${this.props.channelKey}`,
      channel1: [
        {
          channelName: "channel 1",
          message: "",
          userId: 123,
          userDisplayName: "User 1",
          time: "01-01-2019 12:12:12"
        }
      ],
      channel2: [
        {
          channelName: "channel 2",
          message: "",
          userId: 123,
          userDisplayName: "User 1",
          time: "01-01-2019 12:12:12"
        }
      ],
      channel3: [
        {
          channelName: "channel 3",
          message: "",
          userId: 123,
          userDisplayName: "User 1",
          time: "01-01-2019 12:12:12"
        }
      ],
      channel4: [
        {
          channelName: "channel 4",
          message: "",
          userId: 123,
          userDisplayName: "User 1",
          time: "01-01-2019 12:12:12"
        }
      ]
    };
    this.onChange = this.onChange.bind(this);
    this.onChange1 = this.onChange1.bind(this);
  }
  onChange() {
    var index = `channel${this.props.channelKey}`;
    // console.log(index);
    var newStateArray = this.state[index];
    // console.log(newStateArray);
    let obj = {
      channelName: index,
      message: text,
      userId: 123,
      userDisplayName: "User 1",
      time: "01-01-2019 12:12:12"
    };

    newStateArray.push(obj);
    this.setState({ [index]: newStateArray });
    messages.push(text);
    console.log(this.state[index]);
    console.log(messages);
  }

  onChange1(e: any) {
    text = e.target.value;
    console.log(text);
  }
  renderMessage(message: any) {
    return (
      <li>
        <div>{message}</div>
      </li>
    );
  }
  render() {
    let channel = `channel${this.props.channelKey}`;
    let items = this.state[channel];
    let messages = items.map(item => item.message);
    let user = items.map(item => item.userDisplayName);
    console.log(messages);
    return (
      <div>
        <input
          onChange={e => this.onChange1(e)}
          type="text"
          placeholder="Enter your message and press ENTER"
        />
        <Button type="primary" onClick={this.onChange}>
          Send
        </Button>
        <ul>{messages.map(m => this.renderMessage(m))}</ul>
      </div>
    );
  }
}
