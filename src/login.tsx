import React, { Component,PureComponent } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
// @ts-ignore
import { login } from "mattermost-redux/actions/users";
// @ts-ignore
import { getChannels } from "mattermost-redux/actions/channels";
// @ts-ignore
import { logout } from "mattermost-redux/actions/users";
import NewApp from "./App1";

interface LoginProps {
  login: (username: string, password: string) => any;
  users: any;
  getChannels: (teamId: string) => void;
  teamObj: any;
  state: any;
  logout: () => void;
}

class Login extends PureComponent<LoginProps, {}> {
  state = {
    username: "",
    password: "",
    teamId: "",
    data1: false
  };

  handleOnUsername = (e: any) => {
    this.setState({
      username: e.target.value
    });
  };

  handleOnPassword = (e: any) => {
    this.setState({
      password: e.target.value
    });
  };

  login = async () => {
    // logic
    // alert(this.state.username);
    // alert(this.state.password);
    const user = await this.props.login(
      this.state.username,
      this.state.password
    );
    console.log(user);
    this.setState({ data1: user.data });
    console.log("after login ", user);
    await this.getChannel1();
  };
  getChannel1 = async () => {
    console.error("Im in getChannel1", this.state.teamId);
    const channels = await this.props.getChannels(this.state.teamId);
    console.log(channels);
  };
  logout1 = async () => {
    let ob:any = await this.props.logout();
    console.log(ob);
    if(ob.data == true){
      this.setState({data1: false})
    }
  };

  render() {
    console.log("User from props: ", this.props.users);
    console.log("teamObj: ", this.props.teamObj);
    this.state.teamId = Object.keys(this.props.teamObj)[0];
    console.log(this.state.teamId);
    const login_page = (
      <div>
        <input
          type="text"
          placeholder="username"
          onChange={this.handleOnUsername}
        />
        <input
          type="password"
          placeholder="password"
          onChange={this.handleOnPassword}
        />

        <button onClick={this.login}> Login </button>
      </div>
    );
    const chatApp = (
      <div>
        <NewApp state1={this.props.state} logout1={this.logout1}/>
      </div>
    );
    return <div> {this.state.data1 ? chatApp : login_page}</div>;
  }
}

const mapStateToProps = (state: any) => {
  console.log("State: ", state);
  return {
    // entity: state.entities
    state: state,
    users: state.entities.users,
    teamObj: state.entities.teams.teams
  };
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      login,
      getChannels,
      logout
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
